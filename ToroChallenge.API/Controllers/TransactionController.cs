﻿using Microsoft.AspNetCore.Mvc;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Services.Interfaces;

namespace ToroChallenge.API.Controllers
{
    [ApiController]
    [Route("api/sbp")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            this.transactionService = transactionService;
        }

        [HttpPost("events")]
        public async Task<ActionResult> CreateTransaction([FromBody] TransactionDto transaction)
        {
            return Ok(await transactionService.CreateTransactionAsync(transaction));
        }
    }
}
