﻿using Microsoft.AspNetCore.Mvc;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Services.Interfaces;

namespace ToroChallenge.API.Controllers
{
    [ApiController]
    [Route("api/order")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpPost]
        public async Task<ActionResult> CreateOrder([FromBody] OrderDto order)
        {
            return Ok(await orderService.CreateOrderAsync(order));
        }
    }
}
