﻿using Microsoft.AspNetCore.Mvc;
using ToroChallenge.Application.Messages.Request;
using ToroChallenge.Application.Services.Interfaces;

namespace ToroChallenge.API.Controllers
{
    [ApiController]
    [Route("api/stock")]
    public class StockController : ControllerBase
    {
        private readonly IStockService stockService;

        public StockController(IStockService stockService)
        {
            this.stockService = stockService;
        }

        [HttpGet("trends")]
        public async Task<ActionResult> GetStocks([FromQuery] GetStocksQuery request)
        {
            return Ok(await stockService.GetTrendStocksAsync(request.PageSize));
        }

    }
}
