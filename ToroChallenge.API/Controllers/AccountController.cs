﻿using Microsoft.AspNetCore.Mvc;
using ToroChallenge.Application.Services.Interfaces;

namespace ToroChallenge.API.Controllers
{
    [ApiController]
    [Route("api/account")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;

        public AccountController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        [HttpGet("{accountNumber}", Name = "GetAccountById")]
        public async Task<ActionResult> GetAccountByNumber(string accountNumber)
        {
            return Ok(await accountService.GetAccountByNumber(accountNumber));
        }
    }
}
