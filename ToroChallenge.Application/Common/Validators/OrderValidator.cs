﻿using FluentValidation;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Services.Interfaces;
using static ToroChallenge.Infrastructure.Utils.MessagesExtensions;

namespace ToroChallenge.Application.Common.Validators
{
    public class OrderValidator : AbstractValidator<OrderDto>
    {
        public OrderValidator(IAccountService accountService, IStockService stockService)
        {
            RuleFor(x => x.Symbol).NotEmpty().WithMessage(DefaultErrorMessage);
            RuleFor(x => x.Amount).GreaterThan(0).WithMessage(DefaultErrorMessage);
            RuleFor(x => x.AccountNumber).NotEmpty().WithMessage(DefaultErrorMessage);

            RuleFor(x => x).MustAsync(async (order, cancellation) =>
            {
                var account = (await accountService.GetAccountByNumber(order.AccountNumber)).Data;
                var stock = await stockService.GetStockBySymbolAsync(order.Symbol);
                
                if (account == null || stock == null) return false;
                
                order.Stock = stock;
                order.Account = account;
                
                return account.Balance > (order.Amount * stock.CurrentPrice);
            }).WithMessage("Insuficcient Funds");
        }
    }
}
