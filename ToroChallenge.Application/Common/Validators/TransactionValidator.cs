﻿using FluentValidation;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Services.Interfaces;
using static ToroChallenge.Infrastructure.Utils.MessagesExtensions;

namespace ToroChallenge.Application.Common.Validators
{
    public class TransactionValidator : AbstractValidator<TransactionDto>
    {
        public TransactionValidator(IAccountService accountService)
        {
            RuleFor(x => x.Origin.Cpf).NotEmpty().WithMessage(DefaultErrorMessage);
            RuleFor(x => x.Origin.Bank).NotEmpty().WithMessage(DefaultErrorMessage);
            RuleFor(x => x.Origin.Branch).NotEmpty().WithMessage(DefaultErrorMessage);

            RuleFor(x => x.Target.Bank).NotEmpty().WithMessage(DefaultErrorMessage);
            RuleFor(x => x.Target.Branch).NotEmpty().WithMessage(DefaultErrorMessage);
            RuleFor(x => x.Target.Account).NotEmpty().WithMessage(DefaultErrorMessage);
            
            RuleFor(x => x.Amount).GreaterThan(0).WithMessage(DefaultErrorMessage);

            RuleFor(x => x).MustAsync(async (transaction, cancellation) =>
            {
                var account = (await accountService.GetAccountByNumber(transaction.Target.Account)).Data;

                if (account == null) return false;

                return account.User.Cpf == transaction.Origin.Cpf;
            }).WithMessage("Origin CPF doesn't match with the target account CPF");
        }
    }
}
