﻿using AutoMapper;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Application.Common.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<StockDto, Stock>()
                .ForMember(dest => dest.CurrentPrice, opt => opt.MapFrom(e => e.CurrentPrice))
                .ForMember(dest => dest.Symbol, opt => opt.MapFrom(e => e.Symbol.Trim().Normalize()));

            CreateMap<Stock, StockDto>()
                .ForMember(dest => dest.CurrentPrice, opt => opt.MapFrom(e => e.CurrentPrice))
                .ForMember(dest => dest.Symbol, opt => opt.MapFrom(e => e.Symbol.ToUpper()));

            CreateMap<User, UserDto>().ReverseMap();

            CreateMap<Account, AccountDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(dest => dest.Number, opt => opt.MapFrom(e => e.Number))
                .ForMember(dest => dest.Branch, opt => opt.MapFrom(e => e.Branch))
                .ForMember(dest => dest.Balance, opt => opt.MapFrom(e => e.Balance))
                .ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(e => e.CreatedOn))
                .ForMember(dest => dest.Bank, opt => opt.MapFrom(e => e.Bank))
                .ForMember(dest => dest.User, opt => opt.MapFrom(e => e.User));

            CreateMap<TransactionDto, Transaction>()
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(e => e.Amount))
                .ForMember(dest => dest.OriginCpf, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.OriginAccount, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.OriginBank, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.OriginBranch, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.TargetCpf, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.TargetAccount, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.TargetBank, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.TargetBranch, opt => opt.MapFrom(e => e.Origin.Cpf))
                .ForMember(dest => dest.Event, opt => opt.MapFrom(e => e.Event));

            CreateMap<OrderDto, Order>()
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(e => e.Amount))
                .ForMember(dest => dest.Symbol, opt => opt.MapFrom(e => e.Symbol));
        }
    }
}