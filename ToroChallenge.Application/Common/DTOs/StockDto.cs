﻿namespace ToroChallenge.Application.Common.DTOs
{
    public class StockDto
    {
        public string Symbol { get; private set; }
        public decimal CurrentPrice { get; private set; }

        public StockDto(string symbol, decimal currentPrice)
        {
            Symbol = symbol;
            CurrentPrice = currentPrice;
        }
    }
}
