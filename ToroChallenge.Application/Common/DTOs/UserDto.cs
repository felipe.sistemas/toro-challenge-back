﻿namespace ToroChallenge.Application.Common.DTOs
{
    public class UserDto
    {
        public string Name { get; private set; }
        public string Cpf { get; private set; }

        public UserDto(string name, string cpf)
        {
            Name = name;
            Cpf = cpf;
        }
    }
}
