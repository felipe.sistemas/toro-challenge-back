﻿namespace ToroChallenge.Application.Common.DTOs.Abstraction
{
    public class Response <T> : BaseResponse
    {
        public T Data { get; set; }
        
        public Response() { }

        public Response(T data)
        {
            Data = data;
        }

        public Response(T data, string ErrorMessage)
        {
            Data = data;
            Errors = new List<string> { ErrorMessage };
            Success = false;
        }
    }
}
