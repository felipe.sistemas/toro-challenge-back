﻿namespace ToroChallenge.Application.Common.DTOs.Abstraction
{
    public abstract class BaseResponse
    {
        public bool Success { get; set; } = true;
        public IList<string>? Errors { get; set; }

        public void SetResponse(bool success, IList<string>? errors = null, string? ErrorMessage = null)
        {
            Success = success;

            if(errors != null) Errors = errors;

            if(ErrorMessage != null) Errors = new List<string> { ErrorMessage };
        }
    }
}
