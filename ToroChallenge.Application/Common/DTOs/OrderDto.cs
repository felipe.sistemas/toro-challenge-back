﻿using System.Text.Json.Serialization;

namespace ToroChallenge.Application.Common.DTOs
{
    public class OrderDto
    {
        public string AccountNumber { get; set; }
        public string Symbol { get; set; }
        public int Amount { get; set; }

        public OrderDto(string accountNumber, string symbol, int amount)
        {
            AccountNumber = accountNumber;
            Symbol = symbol;
            Amount = amount;
        }

        [JsonIgnore]
        public virtual AccountDto? Account { get; set; }
        [JsonIgnore]
        public virtual StockDto? Stock { get; set; }
    }
}
