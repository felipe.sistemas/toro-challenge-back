﻿namespace ToroChallenge.Application.Common.DTOs
{
    public class TransactionDto
    {
        public string Event { get; private set; }
        public AccountTransactionDto Target { get; private set; }
        public AccountTransactionDto Origin { get; private set; }
        public decimal Amount { get; private set; }

        public TransactionDto(string @event, AccountTransactionDto target, AccountTransactionDto origin, decimal amount)
        {
            Event = @event;
            Target = target;
            Origin = origin;
            Amount = amount;
        }
    }
}
