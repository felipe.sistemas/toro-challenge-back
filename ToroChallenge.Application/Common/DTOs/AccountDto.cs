﻿namespace ToroChallenge.Application.Common.DTOs
{
    public class AccountDto
    {
        public Guid Id { get; private set; }
        public string Bank { get; private set; }
        public string Branch { get; private set; }
        public string Number { get; private set; }
        public decimal Balance { get; private set; }
        public DateTime CreatedOn { get; private set; }
        public virtual UserDto User { get; private set; }

        public AccountDto(string bank, string branch, string number, decimal balance, DateTime createdOn, UserDto user)
        {
            Bank = bank;
            Branch = branch;
            Number = number;
            Balance = balance;
            User = user;
            CreatedOn = createdOn;
        }
    }
}
