﻿namespace ToroChallenge.Application.Common.DTOs
{
    public class AccountTransactionDto
    {
        public string Bank { get; private set; }
        public string Branch { get; private set; }
        public string Account { get; private set; }
        public string? Cpf { get; private set; }

        public AccountTransactionDto(string bank, string branch, string account, string? cpf)
        {
            Bank = bank;
            Branch = branch;
            Account = account;
            Cpf = cpf;
        }
    }
}
