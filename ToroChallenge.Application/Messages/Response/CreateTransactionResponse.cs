﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Common.DTOs.Abstraction;

namespace ToroChallenge.Application.Messages.Response
{
    public class CreateTransactionResponse : Response<TransactionDto>
    {
        public CreateTransactionResponse(TransactionDto data) : base(data)
        {
        }
    }
}
