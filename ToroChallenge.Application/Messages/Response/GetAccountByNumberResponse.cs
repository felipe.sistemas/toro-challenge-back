﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Common.DTOs.Abstraction;

namespace ToroChallenge.Application.Messages.Response
{
    public class GetAccountByNumberResponse : Response<AccountDto?>
    {
        public GetAccountByNumberResponse(AccountDto? account) : base(account)
        {
        }
    }
}
