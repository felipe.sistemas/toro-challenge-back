﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Common.DTOs.Abstraction;

namespace ToroChallenge.Application.Messages.Response
{
    public class CreateOrderResponse : Response<OrderDto>
    {
        public CreateOrderResponse(OrderDto data) : base(data)
        {
        }

        public CreateOrderResponse(OrderDto data, string ErrorMessage) : base(data, ErrorMessage)
        {
        }
    }
}
