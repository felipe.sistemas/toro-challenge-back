﻿namespace ToroChallenge.Application.Messages.Request
{
    public class GetStocksQuery
    {
        public int PageSize { get; set; } = 5;
    }
}
