﻿using AutoMapper;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Messages.Response;
using ToroChallenge.Application.Services.Interfaces;
using static ToroChallenge.Infrastructure.Utils.MessagesExtensions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Application.Services
{
    public class AccountService : IAccountService
    {
        private readonly IAccountRepository accountRepository;
        private readonly IMapper mapper;

        public AccountService(IAccountRepository accountRepository, IMapper mapper)
        {
            this.accountRepository = accountRepository;
            this.mapper = mapper;
        }

        public async Task<GetAccountByNumberResponse> GetAccountByNumber(string accountNumber)
        {
            var account = await accountRepository.GetOneNoTrackingWithPath(x =>
                x.Number.Equals(accountNumber, StringComparison.InvariantCultureIgnoreCase), x => x.User);

            var response = new GetAccountByNumberResponse(mapper.Map<AccountDto>(account));

            if (account == null)
                response.SetResponse(false, ErrorMessage: EntityNotFound("Account"));

            return response;
        }

        public async Task<bool> UpdateAccountBalance(string number, decimal transfer) => await accountRepository.UpdateAccountBalance(number, transfer);
    }
}
