﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Services.Interfaces;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Application.Services
{
    public class StockService : IStockService
    {
        private readonly IMapper mapper;
        private readonly IStockRepository stockRepository;

        public StockService(IMapper mapper, IStockRepository stockRepository)
        {
            this.mapper = mapper;
            this.stockRepository = stockRepository;
        }

        public async Task<StockDto?> GetStockBySymbolAsync(string Symbol)
        {
            var stock = await stockRepository.GetOneNoTracking(x => x.Symbol.Equals(Symbol, StringComparison.InvariantCultureIgnoreCase));

            return mapper.Map<StockDto?>(stock);
        }

        public async Task<IEnumerable<StockDto>> GetTrendStocksAsync(int Size)
        {
            var stocks = await stockRepository.GetAll().OrderBy(x => x.Id).Take(Size).ToListAsync();

            return mapper.Map<IEnumerable<StockDto>>(stocks);
        }
    }
}
