﻿using AutoMapper;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Common.Validators;
using ToroChallenge.Application.Messages.Response;
using ToroChallenge.Application.Services.Interfaces;
using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;
using static ToroChallenge.Infrastructure.Utils.MessagesExtensions;

namespace ToroChallenge.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository orderRepository;
        private readonly IStockService stockService;
        private readonly IAccountService accountService;
        private readonly IMapper mapper;

        public OrderService(IOrderRepository orderRepository, IStockService stockService, IAccountService accountService, IMapper mapper)
        {
            this.orderRepository = orderRepository;
            this.stockService = stockService;
            this.accountService = accountService;
            this.mapper = mapper;
        }

        public async Task<CreateOrderResponse> CreateOrderAsync(OrderDto order)
        {
            var response = new CreateOrderResponse(order);

            if (!await Validate(order, response)) return response;

            var newOrder = new Order(order.Symbol, order.Stock!.CurrentPrice, order.Amount, order.Account!.Id);

            if (await orderRepository.Add(mapper.Map<Order>(newOrder)) == null)
                response = new CreateOrderResponse(order, EntityErrorOnSaving("Order"));
            else
                await accountService.UpdateAccountBalance(order.AccountNumber, -(order.Amount * order.Stock!.CurrentPrice));

            return response;
        }

        /// <summary>
        /// Method to validate the Order
        /// </summary>
        /// <param name="order">Order to be validated. <see cref="OrderDto"/></param>
        /// <param name="response">Optional response to be filled. <see cref="CreateOrderResponse"/></param>
        /// <returns></returns>
        private async Task<bool> Validate(OrderDto order, CreateOrderResponse? response = null)
        {
            var validator = new OrderValidator(accountService, stockService);
            var result = await validator.ValidateAsync(order);

            if (response != null)
                response.SetResponse(result.IsValid, result.Errors.Select(x => x.ErrorMessage).ToList());

            return result.IsValid;
        }
    }
}
