﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Messages.Response;

namespace ToroChallenge.Application.Services.Interfaces
{
    public interface ITransactionService
    {
        Task<CreateTransactionResponse> CreateTransactionAsync(TransactionDto transaction);
    }
}