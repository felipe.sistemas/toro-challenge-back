﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Messages.Response;

namespace ToroChallenge.Application.Services.Interfaces
{
    public interface IAccountService
    {
        Task<GetAccountByNumberResponse> GetAccountByNumber(string accountNumber);
        Task<bool> UpdateAccountBalance(string number, decimal transfer);
    }
}