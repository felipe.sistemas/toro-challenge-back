﻿using ToroChallenge.Application.Common.DTOs;

namespace ToroChallenge.Application.Services.Interfaces
{
    public interface IStockService
    {
        Task<IEnumerable<StockDto>> GetTrendStocksAsync(int Size);
        Task<StockDto?> GetStockBySymbolAsync(string Symbol);
    }
}