﻿using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Messages.Response;

namespace ToroChallenge.Application.Services.Interfaces
{
    public interface IOrderService
    {
        Task<CreateOrderResponse> CreateOrderAsync(OrderDto order);
    }
}
