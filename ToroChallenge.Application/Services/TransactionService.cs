﻿using AutoMapper;
using ToroChallenge.Application.Common.DTOs;
using ToroChallenge.Application.Common.Validators;
using ToroChallenge.Application.Messages.Response;
using ToroChallenge.Application.Services.Interfaces;
using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Application.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly IAccountService accountService;
        private readonly IMapper mapper;
        private readonly ITransactionRepository transactionRepository;

        public TransactionService(IAccountService accountService, IMapper mapper, ITransactionRepository transactionRepository)
        {
            this.accountService = accountService;
            this.mapper = mapper;
            this.transactionRepository = transactionRepository;
        }

        public async Task<CreateTransactionResponse> CreateTransactionAsync(TransactionDto transaction)
        {
            var response = new CreateTransactionResponse(transaction);

            if (!await Validate(transaction, response)) return response;

            if (await accountService.UpdateAccountBalance(transaction.Target.Account, transaction.Amount))
            {
                await transactionRepository.Add(mapper.Map<Transaction>(transaction));
            }

            return response;
        }

        private async Task<bool> Validate(TransactionDto transaction, CreateTransactionResponse? response = null)
        {
            var validator = new TransactionValidator(accountService);
            var result = await validator.ValidateAsync(transaction);

            if (response != null)
                response.SetResponse(result.IsValid, result.Errors.Select(x => x.ErrorMessage).ToList());

            return result.IsValid;
        }
    }
}
