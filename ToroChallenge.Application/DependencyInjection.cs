﻿using Microsoft.Extensions.DependencyInjection;
using ToroChallenge.Application.Services;
using ToroChallenge.Application.Services.Interfaces;

namespace ToroChallenge.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<IStockService, StockService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IOrderService, OrderService>();

            return services;
        }
    }
}
