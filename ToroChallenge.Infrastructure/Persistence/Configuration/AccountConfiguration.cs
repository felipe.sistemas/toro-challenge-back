﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Configuration
{
    public class AccountConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasIndex(x => x.Number).IsUnique();

            builder.Property(x => x.Number).HasMaxLength(6).IsRequired();
            
            builder.Property(x => x.Balance).IsRequired().HasDefaultValue(0);
            
            builder.Property(x => x.Bank).HasMaxLength(3).IsRequired();

            builder.Property(x => x.Branch).HasMaxLength(5).IsRequired();

            builder.HasOne(x => x.User).WithOne(x => x.Account).OnDelete(DeleteBehavior.Cascade);
        }
    }
}