﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Configuration
{
    public class StockConfiguration : IEntityTypeConfiguration<Stock>
    {
        public void Configure(EntityTypeBuilder<Stock> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Symbol)
                .HasMaxLength(6)
                .IsRequired();

            builder.Property(x => x.CurrentPrice)
                .HasPrecision(18, 2)
                .IsRequired();
        }
    }
}