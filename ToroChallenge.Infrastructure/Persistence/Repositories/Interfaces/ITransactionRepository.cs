﻿using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface ITransactionRepository
    {
        Task<Transaction> Add(Transaction transaction);
    }
}