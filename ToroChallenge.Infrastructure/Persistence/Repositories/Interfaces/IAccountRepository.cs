﻿using System.Linq.Expressions;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface IAccountRepository
    {
        Task<Account?> GetOneNoTrackingWithPath<User>(Expression<Func<Account, bool>> expression, Expression<Func<Account, User>> path);
        Task<bool> UpdateAccountBalance(string number, decimal transfer);
    }
}