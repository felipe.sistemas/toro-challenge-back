﻿using System.Linq.Expressions;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface IStockRepository
    {
        IQueryable<Stock> GetAll();
        Task<Stock?> GetOneNoTracking(Expression<Func<Stock, bool>> expression);
    }
}
