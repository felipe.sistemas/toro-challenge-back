﻿using System.Linq.Expressions;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface IBaseRepository<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression);
        Task<TEntity?> GetByIdAsync(Guid id);
        Task<TEntity?> GetOneNoTrackingWithPath<TProperty>(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, TProperty>> path);
        Task<TEntity?> GetOneNoTracking(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> Add(TEntity entity);
    }
}
