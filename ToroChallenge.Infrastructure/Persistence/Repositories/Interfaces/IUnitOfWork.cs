﻿namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        Task CommitAsync();
        ToroChallengeContext Context();
    }
}
