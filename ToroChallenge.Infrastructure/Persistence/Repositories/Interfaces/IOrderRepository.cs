﻿using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        Task<Order> Add(Order transaction);
    }
}
