﻿using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ToroChallengeContext context;

        public UnitOfWork(ToroChallengeContext context)
        {
            this.context = context;
        }

        public async Task CommitAsync()
        {
            using (var transaction = await context.Database.BeginTransactionAsync())
            {
                try
                {
                    await context.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                    throw;
                }
            }
        }

        public ToroChallengeContext Context() => this.context;
    }
}
