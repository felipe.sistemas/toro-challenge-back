﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        public readonly IUnitOfWork _unitOfWork;

        public BaseRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<TEntity> GetAll() => _unitOfWork.Context().Set<TEntity>().AsNoTracking().AsQueryable();

        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> expression) => await GetAll().Where(expression).ToListAsync();
        public async Task<TEntity?> GetByIdAsync(Guid id) => await _unitOfWork.Context().Set<TEntity>().FindAsync(id);
        public async Task<TEntity?> GetOneNoTrackingWithPath<TProperty>(Expression<Func<TEntity, bool>> expression, Expression<Func<TEntity, TProperty>> path)
        {
            return await _unitOfWork.Context().Set<TEntity>().Include(path).AsNoTracking().FirstOrDefaultAsync(expression);
        }

        public async Task<TEntity?> GetOneNoTracking(Expression<Func<TEntity, bool>> expression) =>
            await _unitOfWork.Context().Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(expression);

        public async Task<TEntity> Add(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("Entity not provided");

            await _unitOfWork.Context().AddAsync(entity);

            return entity;
        }
    }
}
