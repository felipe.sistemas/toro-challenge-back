﻿using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure.Persistence.Repositories
{
    public class StockRepository : BaseRepository<Stock>, IStockRepository
    {
        public StockRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
