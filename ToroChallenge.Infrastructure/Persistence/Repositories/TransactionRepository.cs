﻿using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure.Persistence.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }
    }
}
