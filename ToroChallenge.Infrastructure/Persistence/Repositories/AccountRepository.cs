﻿using Microsoft.EntityFrameworkCore;
using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure.Persistence.Repositories
{
    public class AccountRepository : BaseRepository<Account>, IAccountRepository
    {
        public AccountRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public async Task<bool> UpdateAccountBalance(string number, decimal transfer)
        {
            var account = await _unitOfWork.Context().Accounts.FirstOrDefaultAsync(x => x.Number.Equals(number));

            if (account == null) return false;

            account.UpdateBalance(transfer);
            return await _unitOfWork.Context().SaveChangesAsync() > 0;
        }
    }
}
