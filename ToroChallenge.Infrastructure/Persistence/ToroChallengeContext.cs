﻿using Microsoft.EntityFrameworkCore;
using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence
{
    public class ToroChallengeContext : DbContext
    {
        public ToroChallengeContext(DbContextOptions options) : base(options)
        {
        }

        public virtual DbSet<Stock> Stocks { get; set; }
        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
    }
}
