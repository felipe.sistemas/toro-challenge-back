﻿using ToroChallenge.Core.Entities;

namespace ToroChallenge.Infrastructure.Persistence.Seed
{
    public static class ApplicationSeed
    {
        public static async Task SeedSampleDataAsync(ToroChallengeContext context)
        {
            // Seed, if necessary
            if (!context.Stocks.Any())
            {
                #region Stocks
                List<Stock> stocks = new List<Stock>();

                stocks.Add(new Stock("PETR4", 28.44m));
                stocks.Add(new Stock("MGLU3", 25.91m));
                stocks.Add(new Stock("VVAR3", 25.91m));
                stocks.Add(new Stock("SANB11", 40.77m));
                stocks.Add(new Stock("TORO4", 115.98m));
                
                await context.Stocks.AddRangeAsync(stocks);
                #endregion

                User user = new User("12332114303", "John Doe", new Guid("d06cca11-c42f-4c95-b302-9062b0d01d8f"));
                Account account = new Account("033", "03312", "572928", 1000.00m, user.Id);

                await context.Users.AddAsync(user);
                await context.Accounts.AddAsync(account);

                await context.SaveChangesAsync();
            }
        }
    }
}
