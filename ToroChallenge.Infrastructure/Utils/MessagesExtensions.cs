﻿namespace ToroChallenge.Infrastructure.Utils
{
    public static class MessagesExtensions
    {
        public static string DefaultErrorMessage = "{PropertyName} is invalid";
        public static string EntityNotFound(string EntityName) => $"{EntityName} not found";
        public static string EntityErrorOnSaving(string EntityName) => $"Error saving {EntityName}, try again later";
    }
}
