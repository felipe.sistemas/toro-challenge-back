﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ToroChallenge.Infrastructure.Persistence;
using ToroChallenge.Infrastructure.Persistence.Repositories;
using ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;

namespace ToroChallenge.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<ToroChallengeContext>(options =>
                {
                    options.UseInMemoryDatabase("ToroChallenge");
                });
            }
            else
            {
                services.AddDbContext<ToroChallengeContext>(options =>
                options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection"),
                    b => b.MigrationsAssembly(typeof(ToroChallengeContext).Assembly.FullName)));
            }

            services.AddScoped<IStockRepository, StockRepository>();
            services.AddScoped<IAccountRepository, AccountRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
