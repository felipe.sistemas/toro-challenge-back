﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using ToroChallenge.Application.Common.Mappings;
using ToroChallenge.Application.Services;
using ToroChallenge.Application.Services.Interfaces;
using ToroChallenge.Core.Entities;
using ToroChallenge.Infrastructure.Persistence;
using ToroChallenge.Infrastructure.Persistence.Repositories;
using ToroChallenge.Infrastructure.Persistence.Repositories.Abstractions;
using ToroChallenge.Infrastructure.Persistence.Repositories.Interfaces;
using ToroChallenge.Tests.Helpers;

namespace ToroChallenge.Tests
{
    [SetUpFixture]
    public class BaseTesting
    {
        public static IMapper mapper;
        public static ToroChallengeContext context;
        private static IUnitOfWork unitOfWork;

        public static IAccountService accountService;
        public static IAccountRepository accountRepository;

        public static IOrderService orderService;
        public static IOrderRepository orderRepository;

        public static IStockService stockService;
        public static IStockRepository stockRepository;

        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            context = InMemoryContext.CreateContext<ToroChallengeContext>();

            User user = new User("12332114303", "John Doe", new Guid("d06cca11-c42f-4c95-b302-9062b0d01d8f"));
            Account account = new Account("033", "03312", "572928", 1000.00m, user.Id);

            context.Users.Add(user);
            context.Accounts.Add(account);

            context.SaveChanges();

            var serviceProvider = new ServiceCollection()
                    .AddSingleton(context)
                    .BuildServiceProvider();

            mapper = AutoMapperHelper.GetNewMapper(new MappingProfile());

            unitOfWork = new UnitOfWork(context);
            accountRepository = new AccountRepository(unitOfWork);
            accountService = new AccountService(accountRepository, mapper);

            stockRepository = new StockRepository(unitOfWork);
            stockService = new StockService(mapper, stockRepository);

            orderRepository = new OrderRepository(unitOfWork);
            orderService = new OrderService(orderRepository, stockService, accountService, mapper);
        }
    }
}
