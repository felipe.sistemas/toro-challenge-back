﻿namespace ToroChallenge.Tests;

using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToroChallenge.Core.Entities;
using static BaseTesting;

public class OrderTesting
{
    [Test]
    public async Task Create_Order_With_Success()
    {
        List<Stock> stocks = new List<Stock>();

        stocks.Add(new Stock("PETR4", 28.44m));
        stocks.Add(new Stock("MGLU3", 25.91m));
        stocks.Add(new Stock("VVAR3", 25.91m));
        stocks.Add(new Stock("SANB11", 40.77m));
        stocks.Add(new Stock("TORO4", 115.98m));

        var stock = stocks.First(x => x.Symbol.Equals("PETR4"));
        var total = 5 * stock.CurrentPrice;
        var account = await context.Accounts.FirstOrDefaultAsync();

        var accountInitialBalance = account!.Balance;

        await context.Stocks.AddRangeAsync(stocks);

        await context.SaveChangesAsync();

        // 5 stocks
        var response = await orderService.CreateOrderAsync(new Application.Common.DTOs.OrderDto(account.Number, stock.Symbol, 5));

        Assert.IsNotNull(response);
        Assert.IsTrue(response.Success);

        var accountAfterOrder = await accountService.GetAccountByNumber(account.Number);
        Assert.AreEqual(accountInitialBalance - (total), accountAfterOrder.Data!.Balance);
    }
}
