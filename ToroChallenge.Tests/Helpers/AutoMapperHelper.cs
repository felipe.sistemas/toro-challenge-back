﻿using AutoMapper;
using System.Linq;

namespace ToroChallenge.Tests.Helpers
{
    public static class AutoMapperHelper
    {
        public static IMapper GetNewMapper(params Profile[] profiles)
        {
            var config = new MapperConfiguration(config => config.AddProfiles(profiles));

            return config.CreateMapper();
        }
    }
}
