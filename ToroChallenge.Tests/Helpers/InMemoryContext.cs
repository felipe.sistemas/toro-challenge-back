﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Runtime.CompilerServices;

namespace ToroChallenge.Tests.Helpers
{
    public static class InMemoryContext
    {
        public static TContext CreateContext<TContext>([CallerMemberName] string name = "", [CallerFilePath] string path = null) where TContext : DbContext
        {
            var options = new DbContextOptionsBuilder<TContext>()
                .UseInMemoryDatabase(name + path)
                .EnableSensitiveDataLogging()
                .Options;

            return Activator.CreateInstance(typeof(TContext), options) as TContext;
        }
    }
}
