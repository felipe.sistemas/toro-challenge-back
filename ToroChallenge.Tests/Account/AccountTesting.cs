﻿namespace ToroChallenge.Tests;

using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Threading.Tasks;
using ToroChallenge.Core.Entities;
using static BaseTesting;
public class AccountTesting
{
    [Test]
    public async Task Get_Account_With_Success()
    {
        var account = await context.Accounts.FirstOrDefaultAsync();

        var response = await accountService.GetAccountByNumber("572928");

        Assert.IsNotNull(response);
        Assert.IsNull(response.Errors);
        Assert.IsTrue(response.Success);

        var data = response.Data!;

        Assert.AreEqual(data.Number, account!.Number);
        Assert.AreEqual(data.Bank, account!.Bank);
    }
}
