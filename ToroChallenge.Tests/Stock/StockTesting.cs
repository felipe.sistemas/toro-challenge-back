﻿namespace ToroChallenge.Tests;

using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToroChallenge.Core.Entities;
using static BaseTesting;
public class StockTesting
{
    [Test]
    public async Task Get_Stocks_With_Success()
    {
        IEnumerable<Stock> stocks = await context.Stocks.ToListAsync();

        var response = await stockService.GetTrendStocksAsync(5);

        Assert.IsNotNull(response);
        Assert.IsNotEmpty(response);
        Assert.IsNotNull(response.ToList().Any(x => x.Symbol.Equals(stocks.FirstOrDefault())));
    }
}
