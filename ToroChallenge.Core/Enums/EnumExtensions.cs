﻿namespace ToroChallenge.Core.Enums
{
    public static class EnumExtensions
    {
        public static string Value(this TransactionEvent eventType) =>
            eventType switch
            {
                TransactionEvent.TRANSFER => "TRANSFER",
                _ => throw new ArgumentException("Invalid Enum Value")
            };
    }
}
