﻿using System.ComponentModel.DataAnnotations.Schema;
using ToroChallenge.Core.Common;

namespace ToroChallenge.Core.Entities
{
    public class Order : AuditableEntity
    {
        public string Symbol { get; private set; }
        public decimal CurrentPrice { get; private set; }
        public int Amount { get; private set; }
        [ForeignKey("Account")]
        public Guid AccountId { get; private set; }

        public virtual Account? Account { get; set; }

        public Order(string symbol, decimal currentPrice, int amount, Guid accountId)
        {
            Symbol = symbol;
            CurrentPrice = currentPrice;
            Amount = amount;
            AccountId = accountId;
        }
    }
}
