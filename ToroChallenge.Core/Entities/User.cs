﻿using System.ComponentModel.DataAnnotations.Schema;
using ToroChallenge.Core.Common;

namespace ToroChallenge.Core.Entities
{
    public class User : AuditableEntity
    {
        public string Cpf { get; private set; }
        public string Name { get; private set; }
        [ForeignKey("Account")]
        public Guid AccountId { get; private set; }

        public User(string cpf, string name, Guid id) : base()
        {
            Cpf = cpf;
            Name = name;

            if (id != Guid.Empty)
                Id = id;
        }
        public virtual Account? Account { get; private set; }
    }
}
