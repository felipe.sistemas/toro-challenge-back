﻿using ToroChallenge.Core.Common;

namespace ToroChallenge.Core.Entities
{
    public class Transaction : AuditableEntity
    {
        public string Event { get; private set; }
        public string OriginBank { get; private set; }
        public string OriginBranch { get; private set; }
        public string OriginAccount { get; private set; }
        public string? OriginCpf { get; private set; }
        public string TargetBank { get; private set; }
        public string TargetBranch { get; private set; }
        public string TargetAccount { get; private set; }
        public string? TargetCpf { get; private set; }
        public decimal Amount { get; private set; }

        public Transaction(string @event, string originBank, string originBranch, string originAccount, 
            string? originCpf, string targetBank, string targetBranch, string targetAccount, string? targetCpf, decimal amount)
        {
            Event = @event;
            OriginBank = originBank;
            OriginBranch = originBranch;
            OriginAccount = originAccount;
            OriginCpf = originCpf;
            TargetBank = targetBank;
            TargetBranch = targetBranch;
            TargetAccount = targetAccount;
            TargetCpf = targetCpf;
            Amount = amount;
        }
    }
}
