﻿using ToroChallenge.Core.Common;

namespace ToroChallenge.Core.Entities
{
    public class Stock : BaseEntity
    {
        public string Symbol { get; private set; }
        public decimal CurrentPrice { get; private set; }

        public Stock(string symbol, decimal currentPrice)
        {
            Symbol = symbol;
            CurrentPrice = currentPrice;
        }
    }
}
