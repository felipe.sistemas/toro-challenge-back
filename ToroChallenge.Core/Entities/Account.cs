﻿using System.ComponentModel.DataAnnotations.Schema;
using ToroChallenge.Core.Common;

namespace ToroChallenge.Core.Entities
{
    public class Account : AuditableEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; private set; }
        public string Bank { get; private set; }
        public string Branch { get; private set; }
        public string Number { get; private set; }
        public decimal Balance { get; private set; }

        public Account(string bank, string branch, string number, decimal balance, Guid userId) : base()
        {
            Bank = bank;
            Branch = branch;
            Number = number;
            Balance = balance;
            UserId = userId;
        }

        // Navigation properties
        public virtual User User { get; private set; }

        public void UpdateBalance(decimal transfer) => Balance += transfer;
    }
}
